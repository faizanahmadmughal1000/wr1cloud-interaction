﻿using CredentialManagement;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WR1CloudShared.DataModels;
using WR1CloudShared.DataModels.Constants;
using WR1CloudShared.DataModels.Encryption;
using WR1CloudShared.DataModels.Initializer;
using WR1CloudShared.DataModels.Token;
using Outlook = Microsoft.Office.Interop.Outlook;
namespace WR1CloudShared {
    public class Actions {
        private static SecurityController _security = new SecurityController();
        public static Initializer _InitializerObject = new Initializer();
        private static string sToken = "";
        public static bool doGetEmailData() {
            try {
                sToken = "843e95eb-028c-4a66-b6a1-a6283c1a5fd6";
                //Initialize Outlook Instance and Getting Email Address
                Outlook.Application application = new Outlook.Application();
                Outlook.Inspector inspector = application.ActiveInspector();
                Outlook.MAPIFolder selectedFolder = application.ActiveExplorer().CurrentFolder;
                string sPath = selectedFolder.FullFolderPath;
                Object selObject = application.ActiveExplorer().Selection[1];
                GetMyEmail _MyEmail = new GetMyEmail();
                string MyEmail = _MyEmail.doGetMyEmailAddress();
                //Converting Windows Credential Manager into Password String
                string password = "31756869@Aa";
                using (var cred = new Credential()) {
                    cred.Target = MyEmail;
                    cred.Load();
                    if (cred.Load()) {
                        string ss = cred.Password;
                        string nottEncryptedText = _security.Decrypt(password, ss);
                        if (!string.IsNullOrWhiteSpace(sToken)) {
                            //VALIDATE TOKEN
                            TokenResponse oResp = _InitializerObject.doValidateToken(MyEmail, sToken);
                            if (oResp.Success) {
                                //POST
                                if (application.ActiveExplorer().Selection.Count > 0) {
                                    if (selObject is Outlook.MailItem) {
                                        Outlook.MailItem mailItem = (selObject as Outlook.MailItem);
                                        string sSubject = mailItem.Subject;
                                        Outlook.Recipients sReceipt = mailItem.Recipients;
                                        string sSenderEmail = _MyEmail.GetSenderMailItemSMTPAddress(mailItem);
                                        DateTime dEmailDate = mailItem.SentOn;
                                        string Body = mailItem.Body;
                                        string HTMLBody = mailItem.HTMLBody;
                                        string Emailbody = mailItem.Body;
                                        Outlook.Attachments MailAttachments = mailItem.Attachments;
                                        List<Attachments> oAttchList = null;
                                        if (MailAttachments != null && MailAttachments.Count > 0) {
                                            oAttchList = new List<Attachments>();
                                            for (int i = 1; i <= MailAttachments.Count; i++) {
                                                // Retrieve the attachment as a byte array
                                                byte[] BinaryAttachmentData = mailItem.Attachments[i].PropertyAccessor.GetProperty(Constant.PR_ATTACH_DATA_BIN);
                                                string bitString = BitConverter.ToString(BinaryAttachmentData);
                                                FileInfo fi = new FileInfo(MailAttachments[i].FileName);
                                                oAttchList.Add(new Attachments { FileName = MailAttachments[i].FileName, BinaryData = BinaryAttachmentData, Extension = fi.Extension });
                                            }
                                        }
                                        CreateWR1CloudinteractionRequest oReq = new CreateWR1CloudinteractionRequest();
                                        oReq.Token = sToken;
                                        oReq.InteractionName = sSubject;
                                        oReq.ClientEmail = sSenderEmail;
                                        oReq.AssignedUserEmail = MyEmail;
                                        oReq.EmailContent = Emailbody;
                                        oReq.EmaillDateTime = dEmailDate.ToString("yyyy-MM-dd'T'HH:mm:ss.fffffff'Z'");
                                        if (MailAttachments != null && MailAttachments.Count > 0) {
                                            oReq.Attachments = oAttchList;
                                        }
                                        BaseInteractionResponse oResponse = _InitializerObject.CreateInteraction(oReq);
                                        if (oResponse.ResponseStructure.Success) {
                                            string message = "Interaction completed sucessfully, Do you want to open the below link " + System.Environment.NewLine + oResponse.URL.ToString() + " ";
                                            string caption = "WR1 Cloud Interaction";
                                            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                                            System.Windows.Forms.DialogResult result;
                                            result = MessageBox.Show(message, caption, buttons);
                                            if (result == System.Windows.Forms.DialogResult.Yes) {
                                                System.Diagnostics.Process.Start(oResponse.URL.ToString());
                                            }
                                        } else if (!oResponse.ResponseStructure.Success) {
                                            MessageBox.Show(oResponse.ResponseStructure.Message.ToString().ToString());
                                        }
                                    } else {
                                        MessageBox.Show("Select Mail Item");
                                    }
                                    //} else if (selObject is Outlook.ContactItem) {
                                    //    Outlook.ContactItem contactItem = (selObject as Outlook.ContactItem);
                                    //    string sSubject = contactItem.Subject;
                                    //    string sSenderName = "";
                                    //    DateTime dEmailDate = DateTime.MinValue;
                                    //    string Body = contactItem.Body;
                                    //    string Emailbody = contactItem.Body;
                                    //    Outlook.Attachments MailAttachments = contactItem.Attachments;
                                    //    List<Attachments> oAttchList = null;
                                    //    if (MailAttachments != null && MailAttachments.Count > 0) {
                                    //        oAttchList = new List<Attachments>();
                                    //        for (int i = 1; i <= MailAttachments.Count; i++) {
                                    //            // Retrieve the attachment as a byte array
                                    //            byte[] BinaryAttachmentData = contactItem.Attachments[i].PropertyAccessor.GetProperty(Constant.PR_ATTACH_DATA_BIN);
                                    //            string bitString = BitConverter.ToString(BinaryAttachmentData);
                                    //            FileInfo fi = new FileInfo(MailAttachments[i].FileName);
                                    //            oAttchList.Add(new Attachments { FileName = MailAttachments[i].FileName, BinaryData = BinaryAttachmentData, Extension = fi.Extension });
                                    //        }
                                    //    }
                                    //    CreateWR1CloudinteractionRequest oReq = new CreateWR1CloudinteractionRequest();
                                    //    oReq.Token = "";
                                    //    oReq.InteractionName = sSubject;
                                    //    oReq.ClientEmail = sSenderName;
                                    //    oReq.AssignedUserEmail = "";
                                    //    oReq.EmailContent = Emailbody;
                                    //    oReq.EmaillDateTime = dEmailDate.ToString("yyyy-MM-dd'T'HH:mm:ss.fffffff'Z'");
                                    //    if (MailAttachments != null && MailAttachments.Count > 0) {
                                    //        oReq.Attachments = oAttchList;
                                    //    }
                                    //    BaseInteractionResponse oResponse = _InitializerObject.CreateInteraction(oReq);
                                    //    if (oResponse.ResponseStructure.Success) {
                                    //        MessageBox.Show(oResponse.ResponseStructure.Message.ToString() + " " + oResponse.URL.ToString());
                                    //    } else if (!oResponse.ResponseStructure.Success) {
                                    //        MessageBox.Show(oResponse.ResponseStructure.Message.ToString().ToString());
                                    //    }
                                    //} else if (selObject is Outlook.AppointmentItem) {
                                    //    Outlook.AppointmentItem apptItem = (selObject as Outlook.AppointmentItem);
                                    //    string sSubject = apptItem.Subject;
                                    //    Outlook.Recipients sReceipt = apptItem.Recipients;
                                    //    string sReceiptantName = "";
                                    //    if (sReceipt != null && sReceipt.Count > 0) {
                                    //        foreach (Outlook.Recipient recip in sReceipt) {
                                    //            Outlook.PropertyAccessor pa = recip.PropertyAccessor;
                                    //            string smtpAddress = pa.GetProperty(Constant.PR_SMTP_ADDRESS).ToString();
                                    //            sReceiptantName = sReceiptantName + "," + smtpAddress;
                                    //        }
                                    //        sReceiptantName = sReceiptantName.Trim(',');
                                    //    }
                                    //    string sSenderName = "";
                                    //    string Body = apptItem.Body;
                                    //    string Emailbody = apptItem.Body;
                                    //    Outlook.Attachments MailAttachments = apptItem.Attachments;
                                    //    List<Attachments> oAttchList = null;
                                    //    if (MailAttachments != null && MailAttachments.Count > 0) {
                                    //        oAttchList = new List<Attachments>();
                                    //        for (int i = 1; i <= MailAttachments.Count; i++) {
                                    //            // Retrieve the attachment as a byte array
                                    //            byte[] BinaryAttachmentData = apptItem.Attachments[i].PropertyAccessor.GetProperty(Constant.PR_ATTACH_DATA_BIN);
                                    //            string bitString = BitConverter.ToString(BinaryAttachmentData);
                                    //            FileInfo fi = new FileInfo(MailAttachments[i].FileName);
                                    //            oAttchList.Add(new Attachments { FileName = MailAttachments[i].FileName, BinaryData = BinaryAttachmentData, Extension = fi.Extension });
                                    //        }
                                    //    }
                                    //    CreateWR1CloudinteractionRequest oReq = new CreateWR1CloudinteractionRequest();
                                    //    oReq.Token = "";
                                    //    oReq.InteractionName = sSubject;
                                    //    oReq.ClientEmail = sSenderName;
                                    //    oReq.AssignedUserEmail = sReceiptantName;
                                    //    oReq.EmailContent = Emailbody;
                                    //    oReq.EmaillDateTime = "";
                                    //    if (MailAttachments != null && MailAttachments.Count > 0) {
                                    //        oReq.Attachments = oAttchList;
                                    //    }
                                    //    BaseInteractionResponse oResponse = _InitializerObject.CreateInteraction(oReq);
                                    //    if (oResponse.ResponseStructure.Success) {
                                    //        MessageBox.Show(oResponse.ResponseStructure.Message.ToString() + " " + oResponse.URL.ToString());
                                    //    } else if (!oResponse.ResponseStructure.Success) {
                                    //        MessageBox.Show(oResponse.ResponseStructure.Message.ToString().ToString());
                                    //    }
                                    //} else if (selObject is Outlook.TaskItem) {
                                    //    Outlook.TaskItem taskItem = (selObject as Outlook.TaskItem);
                                    //    string sSubject = taskItem.Subject;
                                    //    Outlook.Recipients sReceipt = taskItem.Recipients;
                                    //    string sReceiptantName = "";
                                    //    if (sReceipt != null && sReceipt.Count > 0) {
                                    //        foreach (Outlook.Recipient recip in sReceipt) {
                                    //            Outlook.PropertyAccessor pa = recip.PropertyAccessor;
                                    //            string smtpAddress = pa.GetProperty(Constant.PR_SMTP_ADDRESS).ToString();
                                    //            sReceiptantName = sReceiptantName + "," + smtpAddress;
                                    //        }
                                    //        sReceiptantName = sReceiptantName.Trim(',');
                                    //    }
                                    //    string sSenderName = "";
                                    //    string Body = taskItem.Body;
                                    //    string Emailbody = taskItem.Body;
                                    //    Outlook.Attachments MailAttachments = taskItem.Attachments;
                                    //    List<Attachments> oAttchList = null;
                                    //    if (MailAttachments != null && MailAttachments.Count > 0) {
                                    //        oAttchList = new List<Attachments>();
                                    //        for (int i = 1; i <= MailAttachments.Count; i++) {
                                    //            // Retrieve the attachment as a byte array
                                    //            byte[] BinaryAttachmentData = taskItem.Attachments[i].PropertyAccessor.GetProperty(Constant.PR_ATTACH_DATA_BIN);
                                    //            string bitString = BitConverter.ToString(BinaryAttachmentData);
                                    //            FileInfo fi = new FileInfo(MailAttachments[i].FileName);
                                    //            oAttchList.Add(new Attachments { FileName = MailAttachments[i].FileName, BinaryData = BinaryAttachmentData, Extension = fi.Extension });
                                    //        }
                                    //    }
                                    //    CreateWR1CloudinteractionRequest oReq = new CreateWR1CloudinteractionRequest();
                                    //    oReq.Token = "";
                                    //    oReq.InteractionName = sSubject;
                                    //    oReq.ClientEmail = sSenderName;
                                    //    oReq.AssignedUserEmail = sReceiptantName;
                                    //    oReq.EmailContent = Emailbody;
                                    //    oReq.EmaillDateTime = "";
                                    //    if (MailAttachments != null && MailAttachments.Count > 0) {
                                    //        oReq.Attachments = oAttchList;
                                    //    }
                                    //    BaseInteractionResponse oResponse = _InitializerObject.CreateInteraction(oReq);
                                    //    if (oResponse.ResponseStructure.Success) {
                                    //        MessageBox.Show(oResponse.ResponseStructure.Message.ToString() + " " + oResponse.URL.ToString());
                                    //    } else if (!oResponse.ResponseStructure.Success) {
                                    //        MessageBox.Show(oResponse.ResponseStructure.Message.ToString().ToString());
                                    //    }
                                    //} else if (selObject is Outlook.MeetingItem) {
                                    //    Outlook.MeetingItem meetingItem = (selObject as Outlook.MeetingItem);
                                    //    string sSubject = meetingItem.Subject;
                                    //    Outlook.Recipients sReceipt = meetingItem.Recipients;
                                    //    string sReceiptantName = "";
                                    //    if (sReceipt != null && sReceipt.Count > 0) {
                                    //        foreach (Outlook.Recipient recip in sReceipt) {
                                    //            Outlook.PropertyAccessor pa = recip.PropertyAccessor;
                                    //            string smtpAddress = pa.GetProperty(Constant.PR_SMTP_ADDRESS).ToString();
                                    //            sReceiptantName = sReceiptantName + "," + smtpAddress;
                                    //        }
                                    //        sReceiptantName = sReceiptantName.Trim(',');
                                    //    }
                                    //    string sSenderName = meetingItem.SenderName;
                                    //    DateTime dEmailDate = meetingItem.SentOn;
                                    //    string Body = meetingItem.Body;
                                    //    string Emailbody = meetingItem.Body;
                                    //    Outlook.Attachments MailAttachments = meetingItem.Attachments;
                                    //    List<Attachments> oAttchList = null;
                                    //    if (MailAttachments != null && MailAttachments.Count > 0) {
                                    //        oAttchList = new List<Attachments>();
                                    //        for (int i = 1; i <= MailAttachments.Count; i++) {
                                    //            // Retrieve the attachment as a byte array
                                    //            byte[] BinaryAttachmentData = meetingItem.Attachments[i].PropertyAccessor.GetProperty(Constant.PR_ATTACH_DATA_BIN);
                                    //            string bitString = BitConverter.ToString(BinaryAttachmentData);
                                    //            FileInfo fi = new FileInfo(MailAttachments[i].FileName);
                                    //            oAttchList.Add(new Attachments { FileName = MailAttachments[i].FileName, BinaryData = BinaryAttachmentData, Extension = fi.Extension });
                                    //        }
                                    //    }
                                    //    CreateWR1CloudinteractionRequest oReq = new CreateWR1CloudinteractionRequest();
                                    //    oReq.Token = "";
                                    //    oReq.InteractionName = sSubject;
                                    //    oReq.ClientEmail = sSenderName;
                                    //    oReq.AssignedUserEmail = sReceiptantName;
                                    //    oReq.EmailContent = Emailbody;
                                    //    oReq.EmaillDateTime = dEmailDate.ToString("yyyy-MM-dd'T'HH:mm:ss.fffffff'Z'");
                                    //    if (MailAttachments != null && MailAttachments.Count > 0) {
                                    //        oReq.Attachments = oAttchList;
                                    //    }
                                    //    BaseInteractionResponse oResponse = _InitializerObject.CreateInteraction(oReq);
                                    //    if (oResponse.ResponseStructure.Success) {
                                    //        MessageBox.Show(oResponse.ResponseStructure.Message.ToString() + " " + oResponse.URL.ToString());
                                    //    } else if (!oResponse.ResponseStructure.Success) {
                                    //        MessageBox.Show(oResponse.ResponseStructure.Message.ToString());
                                    //    }
                                    //}
                                }
                            } else {
                                MessageBox.Show(oResp.Message.ToString());
                            }
                        } else {
                            MessageBox.Show("Token is not available in XML File");
                        }
                    } else {
                        return false;
                    }
                }
            } catch (Exception ex) {
                MessageBox.Show(ex.Message.ToString());
            }
            return true;
        }
        public static bool doSaveWindowsCredentialsData(string sConfirmPassword, string sUserName) {
            // Initialize Outlook Instance and Getting Email Address
            Outlook.Application application = new Outlook.Application();
            Outlook.Inspector inspector = application.ActiveInspector();
            Outlook.MAPIFolder selectedFolder = application.ActiveExplorer().CurrentFolder;
            string sPath = selectedFolder.FullFolderPath;
            Object selObject = application.ActiveExplorer().Selection[1];
            GetMyEmail _MyEmail = new GetMyEmail();
            string createdbyEmail = _MyEmail.doGetMyEmailAddress();

            string password = "31756869@Aa";
            string notEncryptedText = sConfirmPassword;
            SecurityController _security = new SecurityController();
            string encryptedText = _security.Encrypt(password, notEncryptedText);
            SecureString sec_strPassword = new SecureString();
            if (encryptedText.Length > 0) {
                SecureString secureString = new SecureString();
                sec_strPassword = convertToSecureString(encryptedText);
            }
            if (!string.IsNullOrWhiteSpace(createdbyEmail)) {
                using (var cred = new Credential()) {
                    cred.Target = createdbyEmail;
                    cred.Username = sUserName;
                    cred.Type = CredentialType.Generic;
                    cred.PersistanceType = PersistanceType.LocalComputer;
                    cred.SecurePassword = sec_strPassword;
                    try {
                        cred.Save();
                        return false;
                    } catch (Exception) {
                    }
                }
            }
            return true;
        }
        public static SecureString convertToSecureString(string strPassword) {
            var secureStr = new SecureString();
            if (strPassword.Length > 0) {
                foreach (var c in strPassword.ToCharArray()) secureStr.AppendChar(c);
            }
            return secureStr;
        }
    }
}
