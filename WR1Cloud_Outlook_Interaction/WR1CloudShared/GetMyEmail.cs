﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WR1CloudShared.DataModels.Constants;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace WR1CloudShared {
    public class GetMyEmail {

        public string doGetMyEmailAddress() {
            Outlook.Application application = new Outlook.Application();
            Outlook.Inspector inspector = application.ActiveInspector();
            Outlook.MAPIFolder selectedFolder = application.ActiveExplorer().CurrentFolder;
            string sPath = selectedFolder.FullFolderPath;
            Object selObject = application.ActiveExplorer().Selection[1];
            string createdbyEmail = "";
            if (selObject is Outlook.MailItem) {
                Outlook.MailItem mailItem = (selObject as Outlook.MailItem);
                createdbyEmail = mailItem.UserProperties.Session.CurrentUser.Address;
                if (application.Session.CurrentUser.AddressEntry.Type == "EX") {
                    createdbyEmail = application.Session.CurrentUser.AddressEntry.GetExchangeUser().PrimarySmtpAddress;
                } else {
                    createdbyEmail = application.Session.CurrentUser.AddressEntry.Address;
                }
            } else if (selObject is Outlook.ContactItem) {
                Outlook.ContactItem contactItem = (selObject as Outlook.ContactItem);
                createdbyEmail = contactItem.UserProperties.Session.CurrentUser.Address;
                if (application.Session.CurrentUser.AddressEntry.Type == "EX") {
                    createdbyEmail = application.Session.CurrentUser.AddressEntry.GetExchangeUser().PrimarySmtpAddress;
                } else {
                    createdbyEmail = application.Session.CurrentUser.AddressEntry.Address;
                }
            } else if (selObject is Outlook.AppointmentItem) {
                Outlook.AppointmentItem apptItem = (selObject as Outlook.AppointmentItem);
                createdbyEmail = apptItem.UserProperties.Session.CurrentUser.Address;
                if (application.Session.CurrentUser.AddressEntry.Type == "EX") {
                    createdbyEmail = application.Session.CurrentUser.AddressEntry.GetExchangeUser().PrimarySmtpAddress;
                } else {
                    createdbyEmail = application.Session.CurrentUser.AddressEntry.Address;
                }
            } else if (selObject is Outlook.TaskItem) {
                Outlook.TaskItem taskItem = (selObject as Outlook.TaskItem);
                createdbyEmail = taskItem.UserProperties.Session.CurrentUser.Address;
                if (application.Session.CurrentUser.AddressEntry.Type == "EX") {
                    createdbyEmail = application.Session.CurrentUser.AddressEntry.GetExchangeUser().PrimarySmtpAddress;
                } else {
                    createdbyEmail = application.Session.CurrentUser.AddressEntry.Address;
                }
            } else if (selObject is Outlook.MeetingItem) {
                Outlook.MeetingItem meetingItem = (selObject as Outlook.MeetingItem);
                createdbyEmail = meetingItem.UserProperties.Session.CurrentUser.Address;
                if (application.Session.CurrentUser.AddressEntry.Type == "EX") {
                    createdbyEmail = application.Session.CurrentUser.AddressEntry.GetExchangeUser().PrimarySmtpAddress;
                } else {
                    createdbyEmail = application.Session.CurrentUser.AddressEntry.Address;
                }
            }
            return createdbyEmail;
        }
        public string GetSenderMailItemSMTPAddress(Outlook.MailItem mail) {
            if (mail == null) {
                throw new ArgumentNullException();
            }
            if (mail.SenderEmailType == "EX") {
                Outlook.AddressEntry sender =
                    mail.Sender;
                if (sender != null) {
                    if (sender.AddressEntryUserType ==
                        Outlook.OlAddressEntryUserType.
                        olExchangeUserAddressEntry
                        || sender.AddressEntryUserType ==
                        Outlook.OlAddressEntryUserType.
                        olExchangeRemoteUserAddressEntry) {
                        Outlook.ExchangeUser exchUser =
                            sender.GetExchangeUser();
                        if (exchUser != null) {
                            return exchUser.PrimarySmtpAddress;
                        } else {
                            return null;
                        }
                    } else {
                        return sender.PropertyAccessor.GetProperty(Constant.PR_SMTP_ADDRESS) as string;
                    }
                } else {
                    return null;
                }
            } else {
                return mail.SenderEmailAddress;
            }
        }
    }
}
