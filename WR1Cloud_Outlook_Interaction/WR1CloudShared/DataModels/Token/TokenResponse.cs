﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WR1CloudShared.DataModels.Token {
    public class TokenResponse {
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}
