﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WR1CloudShared.DataModels {
   public class Attachments {
        public string FileName { get; set; }
        public string Extension { get; set; }
        public byte[] BinaryData { get; set; }
    }
}
