﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WR1CloudShared.DataModels {
    public class CreateWR1CloudinteractionRequest {
        public string Token { get; set; }
        public string InteractionName { get; set; }
        public string ClientEmail { get; set; }
        public string AssignedUserEmail { get; set; }
        public string EmailContent { get; set; }
        public string EmaillDateTime { get; set; }

        List<Attachments> oAttach = new List<Attachments>();
        public List<Attachments> Attachments {
            get { return oAttach; }
            set { oAttach = value; }
        }
    }
}
