﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WR1CloudShared.DataModels {
    public class BaseInteractionResponse {
        InteractionResponse oInteractionResponse = new InteractionResponse();
        public InteractionResponse ResponseStructure {
            get { return oInteractionResponse; }
            set { oInteractionResponse = value; }
        }
        public string URL { get; set; }
    }
}
