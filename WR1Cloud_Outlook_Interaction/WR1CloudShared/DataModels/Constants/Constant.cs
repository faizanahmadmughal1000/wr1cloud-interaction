﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WR1CloudShared.DataModels.Constants {
   public class Constant {
        public const string BASE_REQUEST_URL = "https://osclouddev.isb-global.com/OutlookCommon_API/rest/WR1CloudInteraction/CreateWR1Cloudinteraction";
        public const string PR_SMTP_ADDRESS = "http://schemas.microsoft.com/mapi/proptag/0x39FE001E";
        public const string PR_ATTACH_DATA_BIN = "http://schemas.microsoft.com/mapi/proptag/0x37010102";
    }
}
