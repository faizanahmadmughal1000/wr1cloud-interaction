﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using WR1CloudShared.DataModels;
using WR1CloudShared.DataModels.Constants;
using WR1CloudShared.DataModels.Token;

namespace WR1CloudShared.DataModels.Initializer {
    public class Initializer {
        public BaseInteractionResponse CreateInteraction(CreateWR1CloudinteractionRequest oRequest) {
            var oInteractionResp = new BaseInteractionResponse();
            try {
                string responseJson = string.Empty;
                System.Runtime.Serialization.Json.DataContractJsonSerializer ser = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(CreateWR1CloudinteractionRequest));
                MemoryStream mem = new MemoryStream();
                ser.WriteObject(mem, oRequest);
                string data = Encoding.UTF8.GetString(mem.ToArray(), 0, (int)mem.Length);
                WebClient webClient = new WebClient();
                webClient.Headers["Content-type"] = "application/json";
                webClient.Encoding = Encoding.UTF8;
                responseJson = webClient.UploadString(WR1CloudShared.DataModels.Constants.Constant.BASE_REQUEST_URL, "POST", data);
                oInteractionResp = JsonConvert.DeserializeObject<BaseInteractionResponse>(responseJson);
            } catch (Exception ex) {
                oInteractionResp = null;
            }
            return oInteractionResp;
        }
        public TokenResponse doValidateToken(string sUserEmail,string sToken) {
            try {
                string targetUri = "https://osclouddev.isb-global.com/OutlookCommon_API/rest/WR1CloudInteraction/ValidateUserToken?UserEmail=" + sUserEmail.Trim() + "&Token=" + sToken.Trim() + "";
                var webRequest = (HttpWebRequest)WebRequest.Create(targetUri);
                var webRequestResponse = webRequest.GetResponse();
                Stream Answer = webRequestResponse.GetResponseStream();
                StreamReader _Answer = new StreamReader(Answer);
                var stResponse = _Answer.ReadToEnd();
                var oResponse = JsonConvert.DeserializeObject<TokenResponse>(stResponse);
                return oResponse;
            } catch (WebException ex) {
                string pageContent = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd().ToString();
                return null;
            }
        }
    }
}
