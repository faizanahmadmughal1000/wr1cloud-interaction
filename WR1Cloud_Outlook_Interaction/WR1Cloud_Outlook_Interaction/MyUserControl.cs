﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.Security;
using WR1CloudShared.DataModels.Encryption;
using CredentialManagement;

namespace WR1Cloud_Outlook_Interaction {
    public partial class MyUserControl : UserControl {
        public MyUserControl() {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e) {

        }

        private void button1_Click(object sender, EventArgs e) {

        }

        private void textBox2_TextChanged(object sender, EventArgs e) {

        }

        private void label2_Click(object sender, EventArgs e) {

        }

        private void label1_Click(object sender, EventArgs e) {

        }

        private void MyUserControl_Load(object sender, EventArgs e) {

        }

        private void button_WOC2_Click(object sender, EventArgs e) {
            string sInputPassword, sInputConfirmPassword = "";
            if (InputPassword.Text.Length > 0 && ConfirmPassword.Text.Length > 0) {
                sInputPassword = InputPassword.Text.ToString();
                sInputConfirmPassword = ConfirmPassword.Text.ToString();
                if (sInputPassword != sInputConfirmPassword) {
                    ErrorBox.Text = "Password Doesn't Match..!";
                    ErrorBox.Visible = true;
                } else {
                    bool bReturn = WR1CloudShared.Actions.doSaveWindowsCredentialsData(sInputConfirmPassword, UserName.Text.ToString());
                    if (!bReturn) {
                        for (int i = Globals.ThisAddIn.CustomTaskPanes.Count; i > 0; i--) {
                            Microsoft.Office.Tools.CustomTaskPane myCustomTaskPane;
                            myCustomTaskPane = Globals.ThisAddIn.CustomTaskPanes[i - 1];
                            if (myCustomTaskPane.Title == "WR1Cloud Credentials") {
                                Globals.ThisAddIn.CustomTaskPanes.Remove(myCustomTaskPane);
                                //  myCustomTaskPane.Visible = false;
                            }
                        }
                    }
                }
            } else {
                ErrorBox.Text = "Fields cannot be empty..!";
                ErrorBox.Visible = true;
            }
        }


        private void richTextBox1_TextChanged(object sender, EventArgs e) {

        }

        private void ErrorBox_TextChanged(object sender, EventArgs e) {

        }
    }
}
