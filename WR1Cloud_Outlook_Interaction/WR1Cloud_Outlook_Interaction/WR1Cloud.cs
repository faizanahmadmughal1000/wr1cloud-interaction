﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Tools.Ribbon;
using System.Windows.Forms;

namespace WR1Cloud_Outlook_Interaction {
    public partial class WR1Cloud {
        private void WR1Cloud_Load(object sender, RibbonUIEventArgs e) {
        }
        public void button1_Click(object sender, RibbonControlEventArgs e) {
            bool bReturn = WR1CloudShared.Actions.doGetEmailData();
            if (!bReturn) {
                ThisAddIn.doLoadUserControl();
            }
        }
    }
}
