﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;
using System.Windows.Forms;
using System.IO;
using WR1CloudShared.DataModels;
using WR1CloudShared.DataModels.Initializer;
using System.Collections;
using WR1CloudShared.DataModels.Constants;
using System.Net;
using Newtonsoft.Json;
using WR1CloudShared.DataModels.Token;
using System.Xml;
using System.Reflection;
using CredentialManagement;
using System.Security;
using System.Security.Cryptography;
using WR1CloudShared.DataModels.Encryption;
using System.Drawing;
using Microsoft.Office.Tools.Ribbon;

namespace WR1Cloud_Outlook_Interaction {
    public partial class ThisAddIn {
        private static MyUserControl myUserControl1;
        private static Microsoft.Office.Tools.CustomTaskPane myCustomTaskPane;
        private static string sToken = "";
        Outlook.Explorer currentExplorer = null;
        private bool onStartUp = true;  //Adding this Flag to control button click event on startup, To Keep the button disabled.
        public Microsoft.Office.Tools.Ribbon.RibbonButton IDBox;
        private void ThisAddIn_Startup(object sender, System.EventArgs e) {
            currentExplorer = this.Application.ActiveExplorer();
            currentExplorer.SelectionChange += new Outlook.ExplorerEvents_10_SelectionChangeEventHandler(CurrentExplorer_Event);
        }
        private void ThisAddIn_Shutdown(object sender, System.EventArgs e) {
            // Note: Outlook no longer raises this event. If you have code that 
            //    must run when Outlook shuts down, see http://go.microsoft.com/fwlink/?LinkId=506785
        }
        private void CurrentExplorer_Event() {
            try {
                if (onStartUp) {
                    if (this.Application.ActiveExplorer().Selection.Count > 0) {
                        Globals.Ribbons.WR1Cloud.button1.Enabled = false;
                        onStartUp = false;
                    } else {
                        Globals.Ribbons.WR1Cloud.button1.Enabled = true;
                    }
                } else {
                    if (this.Application.ActiveExplorer().Selection.Count > 0) {
                        Globals.Ribbons.WR1Cloud.button1.Enabled = true;
                    } else {
                        Globals.Ribbons.WR1Cloud.button1.Enabled = false;
                    }
                }
            } catch (Exception ex) {
            }
        }
        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup() {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        public static void doLoadUserControl() {
            myUserControl1 = new MyUserControl();
            myUserControl1.BackColor = Color.Transparent;
            myCustomTaskPane = Globals.ThisAddIn.CustomTaskPanes.Add(myUserControl1, "WR1Cloud Credentials");
            myCustomTaskPane.Visible = true;
            myCustomTaskPane.Width = 390;
            myCustomTaskPane.DockPosition = Office.MsoCTPDockPosition.msoCTPDockPositionRight;
        }
        #endregion
    }
}
